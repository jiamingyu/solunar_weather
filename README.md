# This is a demo Android app pointing to openWeather & Meteoblue apis

## Technical features
### Standard MVVM architect components
- Retrofit2 (V2.6 with coroutine) for network api, with multi service domains endpoints integration
- Persistent tier: Room2 based sqllite access. Store Saved cities for scheduled worker updating informations
- ViewModel: Some tweak on fragments share viewmodel, or each fragment has its own one (like SearchViewModel)
- LiveData
- Navigation framework
### Others
- Dagger2 to defined modules/subcomponents
- Kotlin Coroutine to handle async programming (Sorry, not a CPU intensive based usecase yet)
- Kotlin Channel for search and save city flow, replacing observing pattern. This meet all good points that MVI(ModelViewIntent) pattern is trying to get.

- WorkManager used for handling FusedLocationClient scheduling and result being passed back to caller in async way. Observer handles update location on response, by filtering out viewLifeCycleOwner state change call.

- RecyclerView bind with [PagedListAdapter](https://developer.android.com/reference/android/arch/paging/PagedListAdapter) to handle scrolling down loading with LiveData

- Tweak on extend from MeteoBlueCityBasedPagedListAdapter & inner Holder, to handle recycler view binding for both search and saved city screen. It fits scenario: Clicking flow is identical while card layout definition and icon action differs. (This is a critical tweak from perspective)

Look at MeteoBlueCityPagedListAdapter for search screen

- DataBinding, including binding to RecyclerView.ViewHolder

- [Facebook Stetho](http://facebook.github.io/stetho/) debug tool integration


## View flow and screenshots (More to come)
### Use cases
Start view of Home display default location weather. Botton navigation bar can navigate to Home, Saved cities and Notifications view.

App bar presents couple of functions. Refresh trigger current picked city weather data refresh. Search present search view. Locating triggers current location picked and presenting weather. 

Search View present city searchs. On clicking search(later will update to autocomplete), city candiates will be presented in paginated recycler screen with scrolling action to trigger more loading. You can try most common city name like "springfield" or "washington" in search.

Search View and Saved City flow: On search screen, displaying cities can be added/removed to/from Saved city by click on heart icons. On saved city screen, city can be removed by swiping card on either directions.

Considering limit of the app scope, all saved cities only bound to device storage (sqllite) and account scope does not exist.

[Under design] Notification will present customized notification criterias for saved cities weather changes, and notifications pushed by scheduled interval.

### Screens
Home screen:
![Home](home.png)
Search menu screen
![Search](search.png)
Saved cities screen
![Saved Cities](saved.png)

