package net.solunar.app.common.workers

import android.annotation.SuppressLint
import android.content.Context
import android.location.Location
import android.os.Looper
import android.util.Log
import androidx.concurrent.futures.CallbackToFutureAdapter
import androidx.work.ListenableWorker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.common.util.concurrent.ListenableFuture

class LocationWorker(context: Context, workerParams: WorkerParameters):
    ListenableWorker(context, workerParams) {
    private var fusedLocationProviderClient = FusedLocationProviderClient(context)
    val logTag = "LOCATION_WORKER"

    @SuppressLint("RestrictedApi", "MissingPermission")
    override fun startWork(): ListenableFuture<Result> {
        return CallbackToFutureAdapter.getFuture { completer ->
            val locationRequest = LocationRequest().apply {
                priority = LocationRequest.PRIORITY_HIGH_ACCURACY
                numUpdates = 1
            }

            fusedLocationProviderClient.requestLocationUpdates(locationRequest,
                object : LocationCallback() {
                    override fun onLocationResult(locationResult: LocationResult) {
                        Log.i(logTag, ">>> get location result as ${locationResult.lastLocation}")
                        // Grace message passing on never complete location detection")
                        locationResult.lastLocation?:let {
                            completer.set(Result.failure(workDataOf("failMessage" to "Does not detect location")))
                        }

                        // do work here
                        val d = workDataOf(
                            "lat" to locationResult.lastLocation?.latitude,
                            "lon" to locationResult.lastLocation?.longitude
                        )
                        completer.set(Result.success(d))
                    }
                    fun onLocationChanged(location: Location) {
                        // New location has now been determined
                    }},
                Looper.myLooper()
            )
        }

    }

}
