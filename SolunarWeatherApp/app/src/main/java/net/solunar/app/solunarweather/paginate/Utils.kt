package net.solunar.app.solunarweather.paginate

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import net.solunar.app.solunarweather.room.PER_PAGE_CITIES_LIMIT


fun <T> ViewModel.buildLivePagedList(
    pageLmbda: suspend (String) -> List<T>,
    pageValue: String,
    pageValuelmbda: (String) -> String,
    assignDataSourceLmbda: (GeneralPageKeyedDataSource<T>) -> Unit

): LiveData<PagedList<T>> {

    val config = PagedList.Config.Builder()
        .setPageSize(PER_PAGE_CITIES_LIMIT)
        .setEnablePlaceholders(false)
        .build()

    val dataSourceFactory = object: DataSource.Factory<String, T>() {

        override fun create(): DataSource<String, T> {
            return GeneralPageKeyedDataSource(
                scope =  viewModelScope,
                pageLmbda = pageLmbda,
                pageValue = pageValue,
                pageValuelmbda = pageValuelmbda
            ).also {
                assignDataSourceLmbda.invoke(it)
            }
        }
    }

    return LivePagedListBuilder<String, T>(dataSourceFactory, config).build()
}
