package net.solunar.app.solunarweather.ui.util

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Typeface
import net.solunar.app.solunarweather.R

fun buildBitMap(weatherIconId: String, context: Context): Bitmap {
    val text : String? =findResourceKeyForIconId(weatherIconId, context)

    val bitmap = Bitmap.createBitmap(256, 256, Bitmap.Config.ARGB_4444)
    val canvas = Canvas(bitmap)
    val paint = Paint()
    val weatherFont = Typeface.createFromAsset(
        context!!.assets,
        "fonts/weathericons-regular-webfont.ttf"
    )

    paint.isAntiAlias = true
    paint.isSubpixelText = true
    paint.typeface = weatherFont
    paint.style = Paint.Style.FILL
    paint.textSize = 180f
    paint.textAlign = Paint.Align.CENTER
    canvas.drawText(text!!, 128f, 200f, paint)
    return bitmap
}

fun findResourceKeyForIconId(iconId: String, context: Context): String {
    return when (iconId) {
        "01d" -> context!!.getString(R.string.icon_clear_sky_day)
        "01n" -> context!!.getString(R.string.icon_clear_sky_night)
        "02d" -> context!!.getString(R.string.icon_few_clouds_day)
        "02n" -> context!!.getString(R.string.icon_few_clouds_night)
        "03d" -> context!!.getString(R.string.icon_scattered_clouds)
        "03n" -> context!!.getString(R.string.icon_scattered_clouds)
        "04d" -> context!!.getString(R.string.icon_broken_clouds)
        "04n" -> context!!.getString(R.string.icon_broken_clouds)
        "09d" -> context!!.getString(R.string.icon_shower_rain)
        "09n" -> context!!.getString(R.string.icon_shower_rain)
        "10d" -> context!!.getString(R.string.icon_rain_day)
        "10n" -> context!!.getString(R.string.icon_rain_night)
        "11d" -> context!!.getString(R.string.icon_thunderstorm)
        "11n" -> context!!.getString(R.string.icon_thunderstorm)
        "13d" -> context!!.getString(R.string.icon_snow)
        "13n" -> context!!.getString(R.string.icon_snow)
        "50d" -> context!!.getString(R.string.icon_mist)
        "50n" -> context!!.getString(R.string.icon_mist)
        else -> context!!.getString(R.string.icon_weather_default)
    }
}