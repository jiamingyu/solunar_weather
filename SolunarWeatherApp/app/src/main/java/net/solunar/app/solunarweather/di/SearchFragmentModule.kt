package net.solunar.app.solunarweather.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import net.solunar.app.solunarweather.ui.search.SearchCityFragment
import net.solunar.app.solunarweather.ui.search.SearchViewModel
import javax.inject.Singleton

@Module
interface SearchFragmentModule {

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    fun bindSearchViewModel(searchViewModel: SearchViewModel): ViewModel

}
