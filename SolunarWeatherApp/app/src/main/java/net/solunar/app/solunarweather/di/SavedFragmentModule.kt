package net.solunar.app.solunarweather.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import net.solunar.app.solunarweather.ui.saved.SavedCityViewModel

@Module
interface SavedFragmentModule {
    @Binds
    @IntoMap
    @ViewModelKey(SavedCityViewModel::class)
    fun bindSavedCityViewModel(savedCityViewModel: SavedCityViewModel): ViewModel

}
