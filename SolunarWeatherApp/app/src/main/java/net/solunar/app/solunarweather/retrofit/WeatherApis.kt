package net.solunar.app.solunarweather.retrofit

import net.solunar.app.solunarweather.entity.WeatherResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApis {

    @GET("weather?units=metric&lang=e")
    suspend fun currentByLatLongtitude(
        @Query("appid") appid: String = OpenWeatherRetrofitSingleton.WEATHER_API_APP_ID,
        @Query("lat") lat: Double,
        @Query("lon") lon: Double): WeatherResponse

}
