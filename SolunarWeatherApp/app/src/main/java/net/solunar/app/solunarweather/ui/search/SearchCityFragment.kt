package net.solunar.app.solunarweather.ui.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import net.solunar.app.solunarweather.ActivityViewModelAwareFragment
import net.solunar.app.solunarweather.databinding.FragmentSearchCityBinding
import net.solunar.app.solunarweather.di.ViewModelFactory
import net.solunar.app.solunarweather.entity.Love
import net.solunar.app.solunarweather.entity.SaveCityMessage
import javax.inject.Inject

class SearchCityFragment : ActivityViewModelAwareFragment() {
    @Inject lateinit var viewModelFactory: ViewModelFactory
    private val searchViewModel: SearchViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(SearchViewModel::class.java)
    }
    @Inject lateinit var meteoBlueCityPagedListAdapter: MeteoBlueCityPagedListAdapter

    private lateinit var fragmentSearchBinding: FragmentSearchCityBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        fragmentSearchBinding = FragmentSearchCityBinding.inflate(inflater, container, false)

        // TODO: if want to trigger search each time input new char
        //fragmentSearchBinding.inputSearchCity.addTextChangedListener { newQuery -> ...

        fragmentSearchBinding.searchButton.setOnClickListener {
            val searchText = fragmentSearchBinding.inputSearchCity.text.toString()
            //  trigger search api
            searchText?.let {
                searchViewModel.queryChannel.offer(it)
            }
        }

        fragmentSearchBinding.citySearchResult.apply {
            layoutManager = LinearLayoutManager(context).also {
                addItemDecoration(DividerItemDecoration(context, it.orientation))
            }
            adapter = meteoBlueCityPagedListAdapter
        }

        // Define lambda for city card row click action
        meteoBlueCityPagedListAdapter.holderClickCurryFunc = { view ->
            { city ->
                findNavController().navigate(SearchCityFragmentDirections.search2Home(city))
            }
        }
        // Define lambda for city card save/unsave image button action
        meteoBlueCityPagedListAdapter.submitSaveCityFunc = { city, saveAction ->
            offerChannelLoveAction(SaveCityMessage(city, Love))
        }

        searchViewModel.searchCitiesPaged?.observe(viewLifecycleOwner,
            Observer {
                meteoBlueCityPagedListAdapter.submitList(it)
            }
        )
        return fragmentSearchBinding.root
    }
}
