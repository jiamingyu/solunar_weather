package net.solunar.app.solunarweather.di

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    DbModule::class,
    ViewModelModule::class,
    MainActivityBuilder::class
])
interface WeatherAppComponent : AndroidInjector<AppDaggerWrapper> {

    @Component.Builder
    interface Builder {
        /**
         * add whatever method we want to add to builder.
         * In my case I wanted to add Application to my AppComponent.
         */
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): WeatherAppComponent

    }

    /**
     * This is called by DaggerApplication.injectIfNecessary():
     * AndroidInjector<DaggerApplication>.inject(this) // this therefore must be DaggerApplication.
     *
     * Through the call above,  all dependencies provided by AndroidInjector graph is accessible for 'this' instance
     *
     *
     * Injection cannot be performed in {@link
     * Application#onCreate()} since {@link android.content.ContentProvider}s' {@link
     * android.content.ContentProvider#onCreate() onCreate()} method will be called first and might
     * need injected members on the application. Injection is not performed in the constructor, as
     * that may result in members-injection methods being called before the constructor has completed,
     * allowing for a partially-constructed instance to escape.
     *
     *
     */
    override fun inject(app: AppDaggerWrapper)
}