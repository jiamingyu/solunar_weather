package net.solunar.app.solunarweather.ui.home

import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import dagger.android.support.DaggerFragment
import net.solunar.app.common.workers.LocationWorker
import net.solunar.app.solunarweather.R
import net.solunar.app.solunarweather.databinding.FragmentHomeBinding
import net.solunar.app.solunarweather.di.ViewModelFactory
import net.solunar.app.solunarweather.ui.util.buildBitMap
import javax.inject.Inject


class HomeFragment : DaggerFragment() {
    @Inject lateinit var viewModelFactory: ViewModelFactory

    private val homeViewModel by lazy {
        this.let {
            ViewModelProviders.of(it, viewModelFactory).get(HomeViewModel::class.java)
        }
    }

    val args: HomeFragmentArgs by navArgs()
    private lateinit var fragmentHomeBinding: FragmentHomeBinding

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.activity_main_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.main_menu_search_city -> {
                findNavController().navigate(HomeFragmentDirections.home2SavedCity())
            }
            R.id.main_menu_refresh -> {
                homeViewModel.searchWeather()
            }
            R.id.main_menu_detect_location -> {
                val solunarLocationWorkRequest =
                    OneTimeWorkRequestBuilder<LocationWorker>().build()
//                    .setInputData(myData), TODO: later use this to submit loved cities requests

                // observe location Lookup worker output & enqueue
                context?.let {
                    WorkManager.getInstance(it).apply {
                        getWorkInfoByIdLiveData(solunarLocationWorkRequest.id)
                            .observe(viewLifecycleOwner, Observer { info ->
                                // Observer will be called with state change of viewLifecycleOwner, regardless Livedata update
                                // That is why check detail of info to filter out false positive event make sense
                                info.takeIf { it.state.isFinished }?.outputData?.let {data ->
                                    Log.i("WORKER SUCCESSFUL", "Getting worker output: ${data}")
                                    data.takeIf { it.keyValueMap.containsKey("lat")
                                            && it.keyValueMap.containsKey("lat") }?.let {
                                        homeViewModel.searchWeather(
                                            it.getDouble("lat", 0.0),
                                            it.getDouble("lon", 0.0)
                                        )
                                    }
                                    data.takeIf {it.keyValueMap.containsKey("failMessage")}?.let {
                                        Toast.makeText(context, "Location worker has hiccup & need to take a look", Toast.LENGTH_LONG).show()
                                        Log.e("Broken_location_service", it.getString("failMessage")?:"No valid error info")
                                    }
                                }
                            })
                    }.enqueue(solunarLocationWorkRequest)
                }

            }
        }
        return true
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        fragmentHomeBinding = FragmentHomeBinding.inflate(inflater, container, false)
        if (context == null) {
            return fragmentHomeBinding.root
        }

        return fragmentHomeBinding?.apply {

            toolbarProgress.progress = 10
            toolbarProgress.visibility = View.VISIBLE

            args.cityToSearch?.let {
                homeViewModel?.searchRegion?.value = it
                homeViewModel.searchWeather()
            }

            homeViewModel?.currentWeather?.apply {
                observe(viewLifecycleOwner, Observer {
                    weatherdata = it
                    // Update icon context
                    weatherIcon.background =
                        BitmapDrawable(resources, buildBitMap(it.weather[0].icon, context!!))

                    // TODO: As of now, not find a way to call util function in layout xml, like through
                    // data variable. If figured out, just move this there
                    value?.sys?.also { sys ->
                        sys.sunrise?.let { sr ->
                            homeViewModel.searchRegion.value?.let { city ->
                                sunrise.text =
                                    formatTimePerTimeZone(city.timezone, sr * 1000L)
                            }
                        }
                    }?.sunset?.let { ss ->
                        homeViewModel.searchRegion.value?.let { city ->
                            sunset.text =
                                formatTimePerTimeZone(city.timezone, ss * 1000L)
                        }
                    }
                    // last update time
                    lastUpdateTime.text = "Last update " + formatTimePerTimeZone(
                        null,
                        System.currentTimeMillis()
                    )
                })
            }
            homeViewModel?.loadingInProgress.observe(viewLifecycleOwner, Observer { loading ->
                when (loading) {
                    true -> toolbarProgress.visibility = View.VISIBLE
                    false -> toolbarProgress.visibility = View.GONE
                }
            })

            initializeTextView(this)
            this@HomeFragment.setHasOptionsMenu(true)
            checkAccessLocationPermission()
        }.root
    }
}
