package net.solunar.app.solunarweather.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import net.solunar.app.solunarweather.ui.home.HomeFragment
import net.solunar.app.solunarweather.ui.saved.SavedCityFragment
import net.solunar.app.solunarweather.ui.search.SearchCityFragment

@Module
interface FragmentsBuilder {
    // Each fragment should have its own sub-component, declared by @ContributeAndroidInjector

    @ContributesAndroidInjector(modules = [HomeFragmentModule::class])
    fun providesHomeFragment():HomeFragment

    @ContributesAndroidInjector(modules = [SearchFragmentModule::class])
    fun providesSearchCityFragment(): SearchCityFragment

    @ContributesAndroidInjector(modules = [SavedFragmentModule::class])
    fun providesSavedCityFragment(): SavedCityFragment
}