package net.solunar.app.solunarweather.room

import androidx.room.Database
import androidx.room.RoomDatabase
import net.solunar.app.solunarweather.entity.meteoblue.City

@Database(version= 2, entities = [City::class])
abstract  class SolunarWeaterDb : RoomDatabase() {
    abstract fun cityDao(): CityDao
}
