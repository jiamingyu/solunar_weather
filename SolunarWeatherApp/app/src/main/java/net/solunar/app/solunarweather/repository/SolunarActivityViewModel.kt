package net.solunar.app.solunarweather.repository

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.withContext
import net.solunar.app.solunarweather.entity.Love
import net.solunar.app.solunarweather.entity.SaveCityMessage
import net.solunar.app.solunarweather.entity.UnLove
import net.solunar.app.solunarweather.entity.meteoblue.City
import net.solunar.app.solunarweather.room.PER_PAGE_CITIES_LIMIT
import net.solunar.app.solunarweather.room.SolunarWeaterDb
import net.solunar.app.solunarweather.ui.search.SearchViewModel
import javax.inject.Inject


class SolunarActivityViewModel @Inject constructor(): ViewModel() {
    @Inject lateinit var solunarWeaterDb: SolunarWeaterDb


    var saveCityChannel = ConflatedBroadcastChannel<SaveCityMessage>()

    init {
        saveCityChannel.asFlow().debounce(SearchViewModel.QUERY_DEBOUNCE)
            .onEach {msg ->
                when (msg.action) {
                    Love -> withContext(Dispatchers.IO) {
                        solunarWeaterDb.cityDao().insertCity(msg.city)
                    }
                    UnLove -> withContext(Dispatchers.IO) {
                        solunarWeaterDb.cityDao().deleteCity(msg.city)
                    }
                }

            }.launchIn(viewModelScope)
    }

    suspend fun getPagedCities(pageStr: String): List<City> = withContext(Dispatchers.IO) {
        val offsetPosition = (Integer.parseInt(pageStr)-1) * PER_PAGE_CITIES_LIMIT
        solunarWeaterDb.cityDao().getPagedCities(offsetPosition)
    }


}