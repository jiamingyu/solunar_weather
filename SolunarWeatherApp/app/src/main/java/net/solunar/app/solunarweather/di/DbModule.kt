package net.solunar.app.solunarweather.di

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import net.solunar.app.solunarweather.room.SolunarWeaterDb
import javax.inject.Singleton

@Module
class DbModule {
    @Singleton
    @Provides
    fun provideDb(app: Application): SolunarWeaterDb =
        Room.databaseBuilder(app, SolunarWeaterDb::class.java, "WeatherDB")
            .fallbackToDestructiveMigration() // For scenario that don’t want to provide migrations and you specifically want your database to be cleared
            .build()
}
