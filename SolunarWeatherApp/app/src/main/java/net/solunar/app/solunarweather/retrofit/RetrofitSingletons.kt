package net.solunar.app.solunarweather.retrofit

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class OpenWeatherRetrofitSingleton {
    companion object {
        const val OPEN_WEATHER_BASE_URL = "https://api.openweathermap.org/data/2.5/"
        const val WEATHER_API_APP_ID = "1487dd8a93bfd85d278d9ac8dcfee94c"

        val gson = GsonBuilder().setLenient().create()
        val INSTANCE =  Retrofit.Builder()
            .baseUrl(OPEN_WEATHER_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
        fun apis(): WeatherApis = INSTANCE.create(WeatherApis::class.java)
    }
}

class MeteoblueRetrofitSingleton {
    companion object {
        const val   METEOBLUE_BASE_URL = "https://www.meteoblue.com/en/server/"
        val gson = GsonBuilder().setLenient().create()
        val INSTANCE =  Retrofit.Builder()
            .baseUrl(METEOBLUE_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
        fun apis(): MeteoblueApis = INSTANCE.create(MeteoblueApis::class.java)
    }
}