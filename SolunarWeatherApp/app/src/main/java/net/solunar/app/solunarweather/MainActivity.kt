package net.solunar.app.solunarweather

import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.facebook.stetho.Stetho
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import net.solunar.app.solunarweather.di.ViewModelFactory
import net.solunar.app.solunarweather.repository.SolunarActivityViewModel
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), CoroutineScope by MainScope() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    val solunarActivityViewModel: SolunarActivityViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(SolunarActivityViewModel::class.java)
    }

    private lateinit var appBarConfiguration : AppBarConfiguration

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Enable Stetho: chrome://inspect/#devices
        Stetho.initializeWithDefaults(this);

        setContentView(R.layout.activity_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val navController = navCtlLambda.invoke()
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(setOf(
                R.id.navigation_home, R.id.navigation_saved_city, R.id.navigation_notifications))
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onDestroy() {
        Job().cancel()
        super.onDestroy()
    }

    override fun onSupportNavigateUp(): Boolean {
        return navCtlLambda.invoke().navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
        //Note: By reading impl, I believe the alternative of calling: navCtlLambda.invoke().navigateUp(),
        // is diff in impl approach & not gurantee behavior defined in nav configurations.
    }

    val navCtlLambda : () -> NavController = {findNavController(R.id.nav_host_fragment)}
}
