package net.solunar.app.solunarweather.ui.search

import android.view.View
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import net.solunar.app.solunarweather.entity.meteoblue.City

abstract class MeteoBlueCityBasedPagedListAdapter<T: City, U: RecyclerView.ViewHolder>(
    diffUtilCallBack: DiffUtilCallBack
) : PagedListAdapter<City, RecyclerView.ViewHolder>(DiffUtilCallBack()){
    lateinit var holderClickCurryFunc : (View) -> (City) -> Unit

    final override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        getItem(position)?.let {city ->
            if (holder !is BaseViewHolder) {
                return@let
            }
            holder.apply {
                bind(city)

                itemView.setOnClickListener { view ->
                    if (::holderClickCurryFunc.isInitialized) {
                        holderClickCurryFunc(view)(city)
                    }
                }
                HolderActionCustomize(city)
            }
        }
    }

    abstract fun  BaseViewHolder.HolderActionCustomize(t: City)

    abstract class BaseViewHolder(val viewDataBinding: ViewDataBinding): RecyclerView.ViewHolder(viewDataBinding.root) {
        abstract fun bind(city: City)
    }
}
