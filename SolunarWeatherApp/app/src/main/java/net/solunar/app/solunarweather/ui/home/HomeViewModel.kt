package net.solunar.app.solunarweather.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import net.solunar.app.solunarweather.entity.WeatherResponse
import net.solunar.app.solunarweather.entity.meteoblue.City
import net.solunar.app.solunarweather.pref.NETWORK_DELAY_FOR_DEBUG
import net.solunar.app.solunarweather.retrofit.OpenWeatherRetrofitSingleton
import javax.inject.Inject

class HomeViewModel @Inject constructor(): ViewModel() {
    val searchRegion: MutableLiveData<City> = MutableLiveData<City>().apply {
        value = City()
    }

    val currentWeather: MutableLiveData<WeatherResponse> = MutableLiveData<WeatherResponse>()
    var loadingInProgress = MutableLiveData<Boolean>(true)



    init {
        searchWeather()
    }

// Note: here is the original solution. TBD
//    val currentWeather: MutableLiveData<WeatherResponse> = liveData<WeatherResponse>(Dispatchers.IO) {
//        val x = OpenWeatherRetrofitSingleton.apis().currentByLatLongtitude(lat=37.4419, lon=-122.143)
//        emit(x)
//    } as MutableLiveData<WeatherResponse>

    fun searchWeather(lat: Double? = null, lon: Double? = null) {
        loadingInProgress.postValue(true)

        viewModelScope.launch(Dispatchers.IO) {
            lat?.let{let ->
                lon?.let {lon ->
                    searchRegion.postValue(City(lat=lat, lon=lon))
                    // Observed racing condition between value post and api call reading
                    // post is fire & forget therefore has no handler to track done
                    // Not a perfect solution. Even Dispatchers not good on IO. Open to change
                    delay(500)
                }
            }

            val x = OpenWeatherRetrofitSingleton.apis()
                .currentByLatLongtitude(lat = searchRegion.value?.lat!!, lon = searchRegion.value?.lon!!)
            delay(NETWORK_DELAY_FOR_DEBUG) //TODO: this one will be refacted to read from debugging setting
            currentWeather.postValue(x)
            loadingInProgress.postValue(false) //TODO: find a coroutine way:    withContext(Dispatchers.Main) { ...
        }
    }
}