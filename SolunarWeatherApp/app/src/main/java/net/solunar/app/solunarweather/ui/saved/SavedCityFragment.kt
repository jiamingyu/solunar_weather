package net.solunar.app.solunarweather.ui.saved

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import net.solunar.app.solunarweather.ActivityViewModelAwareFragment
import net.solunar.app.solunarweather.databinding.FragmentSavedCityBinding
import net.solunar.app.solunarweather.di.ViewModelFactory
import net.solunar.app.solunarweather.entity.SaveCityMessage
import net.solunar.app.solunarweather.entity.UnLove
import javax.inject.Inject

class SavedCityFragment : ActivityViewModelAwareFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    @Inject
    lateinit var savedCityPagedListAdapter: SavedCityPagedListAdapter

    lateinit var fragmentSavedCityBinding: FragmentSavedCityBinding

    private val savedCityViewModel: SavedCityViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(SavedCityViewModel::class.java)
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        fragmentSavedCityBinding = FragmentSavedCityBinding.inflate(inflater, container, false)

        fragmentSavedCityBinding.savedCities.apply {
            layoutManager = LinearLayoutManager(context).also {
                addItemDecoration(DividerItemDecoration(context, it.orientation))
            }
            adapter = savedCityPagedListAdapter

            savedCityPagedListAdapter?.let { adpt ->
                // swipe actions
                val swipeHandler = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
                    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                        val holderCity = (viewHolder as SavedCityPagedListAdapter.SavedCityViewHolder).viewBinding.city
                        holderCity?.let {city ->
                            offerChannelLoveAction(SaveCityMessage(city, UnLove))
                        }
                        savedCityViewModel.deferredRefreshSavedCities(1000L)
                    }
                    override fun onMove(
                        recyclerView: RecyclerView,
                        viewHolder: RecyclerView.ViewHolder,
                        target: RecyclerView.ViewHolder
                    ): Boolean = false
                }
                val itemTouchHelper = ItemTouchHelper(swipeHandler)
                itemTouchHelper.attachToRecyclerView(this)

                // click actions
                adpt.holderClickCurryFunc = { view ->
                    { city ->
                        findNavController().navigate(SavedCityFragmentDirections.saved2Home(city))
                    }
                }
            }
        }

        savedCityViewModel.savedCitiesPaged?.observe(viewLifecycleOwner,
            Observer { pagedListCities ->
                savedCityPagedListAdapter.submitList(pagedListCities)
            }
        )

        return fragmentSavedCityBinding.root
    }
}
