package net.solunar.app.solunarweather.entity.meteoblue

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

data class CitySearchResponse (

    val query : String,
    val iso2 : String,
    val currentPage : Int,
    val itemsPerPage : Int,
    val pages : Int,
    val count : Int,
    val orderBy : String,
    val lat : String,
    val lon : String,
    val radius : Int,
    val type : String,
    val results : List<City>
)

@Entity(tableName = "loved_cities")
@Parcelize
data class City (
    @PrimaryKey
    val id : Int = 0,
    val name : String = "",
    val iso2 : String = "",
    val country : String = "",
    val admin1 : String = "",
    val lat : Double = 37.4419,
    val lon : Double = -122.143,
    val asl : Int = 0,
    val timezone : String = "",
    val population : Int = 0,
    val distance : Double = 0.0,
    val icao : String = "",
    val iata : String = "",
//    val postcodes : List<String> = listOf(), //TODO: convert needed
    val featureClass : String = "",
    val featureCode : String = "",
    val url : String = "",
    var savedByUser: Boolean = false
): Parcelable
