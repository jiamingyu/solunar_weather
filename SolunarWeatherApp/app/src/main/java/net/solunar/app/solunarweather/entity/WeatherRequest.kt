package net.solunar.app.solunarweather.entity

data class WeatherRequest(val lat: String, val lon: String, val units: String, val lang: String){
}