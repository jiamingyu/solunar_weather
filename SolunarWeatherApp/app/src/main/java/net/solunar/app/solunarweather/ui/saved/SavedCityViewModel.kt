package net.solunar.app.solunarweather.ui.saved

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.DataSource
import androidx.paging.PagedList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.solunar.app.solunarweather.entity.meteoblue.City
import net.solunar.app.solunarweather.paginate.buildLivePagedList
import net.solunar.app.solunarweather.repository.SolunarActivityViewModel
import javax.inject.Inject

class SavedCityViewModel @Inject constructor(): ViewModel() {
    @Inject lateinit var savedcityMediator: SolunarActivityViewModel

    var savedCitiesDataSource: DataSource<String, City>? = null
    var savedCitiesPaged: LiveData<PagedList<City>> = buildLivePagedList (
        pageLmbda = {page ->
            withContext(Dispatchers.IO) {
                savedcityMediator.getPagedCities(page)
            }
        },
        pageValue = "1",
        pageValuelmbda = {pgStr ->
            (Integer.parseInt(pgStr)+1).toString()
        },
        assignDataSourceLmbda = { gds ->
            savedCitiesDataSource = gds
        }
    )

    /**
     * Reload data from Dao considering avoiding racing issue from CRUD
     */
    fun deferredRefreshSavedCities(deferredMilSecond: Long) {
        viewModelScope.launch {
            delay(deferredMilSecond)
            savedCitiesDataSource?.invalidate()
        }
    }

}
