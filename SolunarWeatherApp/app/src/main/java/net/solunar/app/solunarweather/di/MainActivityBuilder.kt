package net.solunar.app.solunarweather.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import net.solunar.app.solunarweather.MainActivity

@Module
interface MainActivityBuilder {

    @ContributesAndroidInjector(modules = [MainActivityModule::class, FragmentsBuilder::class])
    fun provideMainActivity(): MainActivity
}