package net.solunar.app.solunarweather.retrofit

import net.solunar.app.solunarweather.entity.meteoblue.CitySearchResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface MeteoblueApis {

    @GET("search/query3/")
    suspend fun findCityByName(@Query("query") query: String, @Query("page") page: String = "1"): CitySearchResponse
}
