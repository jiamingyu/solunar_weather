package net.solunar.app.solunarweather.room

import androidx.room.*
import net.solunar.app.solunarweather.entity.meteoblue.City

const val PER_PAGE_CITIES_LIMIT = 10

@Dao
interface CityDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCity(city: City)

    @Query("select * from loved_cities")
    fun getAll(): List<City>

    @Query("select * from loved_cities limit $PER_PAGE_CITIES_LIMIT offset :offset ")
    fun getPagedCities(offset: Int):List<City>

    @Delete
    fun deleteCity(city: City)

}
