package net.solunar.app.solunarweather.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.city_card.view.*
import net.solunar.app.solunarweather.R
import net.solunar.app.solunarweather.databinding.CityCardBinding
import net.solunar.app.solunarweather.entity.Love
import net.solunar.app.solunarweather.entity.LoveAction
import net.solunar.app.solunarweather.entity.UnLove
import net.solunar.app.solunarweather.entity.meteoblue.City
import javax.inject.Inject

class MeteoBlueCityPagedListAdapter @Inject constructor() : MeteoBlueCityBasedPagedListAdapter<City, RecyclerView.ViewHolder>(DiffUtilCallBack()) {
    lateinit var submitSaveCityFunc : (City, LoveAction) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val cityCardBinding = CityCardBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CityViewHolder(cityCardBinding)
    }

    override fun BaseViewHolder.HolderActionCustomize(city: City)
    {
        val image = when (city.savedByUser) {
            true -> R.drawable.ic_heart_fill_24dp
            false -> R.drawable.ic_heart_24dp
        }

        (itemView.adpt_loved_city_button as ImageView).setImageResource(image)

        itemView.adpt_loved_city_button.setOnClickListener { loveImg ->
            // Queue msg and forget
            when (city.savedByUser) {
                true -> {
                    city.savedByUser = false
                    submitSaveCityFunc(city, UnLove)
                    (itemView.adpt_loved_city_button as ImageView).setImageResource(R.drawable.ic_heart_24dp)
                }
                false -> {
                    city.savedByUser = true
                    submitSaveCityFunc(city, Love)
                    (itemView.adpt_loved_city_button as ImageView).setImageResource(R.drawable.ic_heart_fill_24dp)
                }
            }
        }
    }


    class CityViewHolder(val viewBinding: CityCardBinding): BaseViewHolder(viewBinding) {
        override fun bind(city: City) {
            viewBinding.city = city
            viewBinding.adptWeatherDesc.text = "${city.admin1}, ${city.country}" // TODO: I don't find a way to define this in template

        }
    }
}

class DiffUtilCallBack: DiffUtil.ItemCallback<City>() {
    override fun areItemsTheSame(oldItem: City, newItem: City): Boolean {
        return oldItem.lat == newItem.lat && oldItem.lon == newItem.lon
    }

    override fun areContentsTheSame(oldItem: City, newItem: City): Boolean {
        return oldItem.lat == newItem.lat && oldItem.lon == newItem.lon
    }
}
