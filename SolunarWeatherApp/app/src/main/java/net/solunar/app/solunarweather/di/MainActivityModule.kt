package net.solunar.app.solunarweather.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import net.solunar.app.solunarweather.repository.SolunarActivityViewModel

@Module
interface MainActivityModule {

    @Binds
    @IntoMap
    @ViewModelKey(SolunarActivityViewModel::class)
    fun bindsSavedCityMediator(solunarActivityViewModel: SolunarActivityViewModel): ViewModel

}