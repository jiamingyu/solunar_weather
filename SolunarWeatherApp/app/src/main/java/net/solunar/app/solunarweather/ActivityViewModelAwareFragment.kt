package net.solunar.app.solunarweather

import dagger.android.support.DaggerFragment
import net.solunar.app.solunarweather.entity.SaveCityMessage

open class ActivityViewModelAwareFragment : DaggerFragment() {
    fun offerChannelLoveAction(msg: SaveCityMessage) {
        (activity as net.solunar.app.solunarweather.MainActivity)
            .solunarActivityViewModel.saveCityChannel.offer(msg)
    }
}