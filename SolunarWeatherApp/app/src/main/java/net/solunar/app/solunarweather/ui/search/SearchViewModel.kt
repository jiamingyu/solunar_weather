package net.solunar.app.solunarweather.ui.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.DataSource
import androidx.paging.PagedList
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.solunar.app.solunarweather.entity.Love
import net.solunar.app.solunarweather.entity.SaveCityMessage
import net.solunar.app.solunarweather.entity.UnLove
import net.solunar.app.solunarweather.entity.meteoblue.City
import net.solunar.app.solunarweather.paginate.buildLivePagedList
import net.solunar.app.solunarweather.retrofit.MeteoblueRetrofitSingleton
import net.solunar.app.solunarweather.room.SolunarWeaterDb
import javax.inject.Inject


class SearchViewModel @Inject constructor(): ViewModel() {

    val queryChannel = ConflatedBroadcastChannel(DEFAULT_SEARCH_KEY)

    private var meteoblueDataSource:  DataSource<String, City>? = null
    var searchCitiesPaged: LiveData<PagedList<City>> = buildLivePagedList(
        pageLmbda = {page ->
            withContext(Dispatchers.IO) {
                MeteoblueRetrofitSingleton.apis().findCityByName(queryChannel?.valueOrNull.orEmpty(), page)?.results
            }
        },
        pageValue = "1",
        pageValuelmbda = {pgStr ->
            (Integer.parseInt(pgStr)+1).toString()
        },
        assignDataSourceLmbda = { gds ->
            meteoblueDataSource = gds
        }
    )

    init {
        queryChannel.asFlow().debounce(QUERY_DEBOUNCE)
            .onEach{
                if (!it.equals(DEFAULT_SEARCH_KEY)) {
                    meteoblueDataSource?.invalidate()
                }
            }
            .launchIn(viewModelScope)


    }

    companion object {
        const val QUERY_DEBOUNCE = 500L
        const val DEFAULT_SEARCH_KEY = "springfield"
    }
}


