package net.solunar.app.solunarweather.paginate

import androidx.paging.PageKeyedDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class GeneralPageKeyedDataSource<T>(
    private val scope: CoroutineScope,
    private val pageLmbda: suspend (String) -> List<T>,
    private val pageValue: String,
    private val pageValuelmbda: (String) -> String
): PageKeyedDataSource<String, T>() {

    override fun loadInitial(
        params: LoadInitialParams<String>,
        callback: LoadInitialCallback<String, T>
    ) {
        params.requestedLoadSize //TODO: ?? initial on this loadSize ?

        scope.launch{

                val data = pageLmbda.invoke(pageValue)

                callback.onResult( data as MutableList<T>, null, pageValuelmbda.invoke(pageValue)) // if both nulls, b4/after call will not happen
        }
    }

    override fun loadAfter(
        params: LoadParams<String>,
        callback: LoadCallback<String, T>
    ) {
        scope.launch {
            val data =  pageLmbda.invoke(params.key)
            callback.onResult( data as MutableList<T>, pageValuelmbda.invoke(params.key) ) // if both nulls, b4/after call will not happen
        }
    }

    override fun loadBefore(
        params: LoadParams<String>,
        callback: LoadCallback<String, T>
    ) {
        // skip for most cases
    }

    override fun invalidate() {
        super.invalidate()

        // TODO:(Discussion) open discussion: although each coroutine is light, we still want to kill only children scopes of discarding dataSource instance,
        //  while not impacting the newly initing datasource called child scopes.
        //  Single job reference pointing to init/loadAfter launched coroutines is not a solution since each page will have the ref missing the past one.
        //  ON the other hand, It should not be a big deal since each job is so small and will be killed after execution done
        //  DONOT:
        //  A. Can not cancel viewmodel scopehere like: scope.cancel(). This is causing bug: kill VM scope leading to fail of launch new datasource. this way we cancelled VM coroutine.
        //  B. Can not cancel all its children, since new creating jobs for new search string based datasource will also be cancelled
    }

}
