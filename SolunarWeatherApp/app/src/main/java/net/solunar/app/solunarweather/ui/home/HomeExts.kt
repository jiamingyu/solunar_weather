package net.solunar.app.solunarweather.ui.home

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.text.format.DateFormat
import androidx.core.app.ActivityCompat
import net.solunar.app.solunarweather.MainActivity
import net.solunar.app.solunarweather.databinding.FragmentHomeBinding
import java.util.*

private const val REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE = 34

fun HomeFragment.checkAccessLocationPermission() {
    val p = PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
        this.context!!,
        Manifest.permission.ACCESS_FINE_LOCATION
    )

    if (!p) {
        ActivityCompat.requestPermissions(
            this.activity as MainActivity,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            REQUEST_FOREGROUND_ONLY_PERMISSIONS_REQUEST_CODE
        )
    }
}

fun HomeFragment.formatTimePerTimeZone(timeZone: String?, timeInMilSecond: Long): String {
    val formatter = DateFormat.getTimeFormat(context)
    timeZone?.let {
        formatter.timeZone = TimeZone.getTimeZone(it)
    }
    return formatter.format(Date(timeInMilSecond))
}



fun HomeFragment.initializeTextView(fragmentHomeBinding: FragmentHomeBinding) {
    /**
     * Create typefaces from Asset
     */
    val weatherFontIcon = Typeface.createFromAsset(
        this.activity?.assets,
        "fonts/weathericons-regular-webfont.ttf"
    )

    val textViewsIcon = listOf(
        fragmentHomeBinding.iconRegion,
        fragmentHomeBinding.iconWind,
        fragmentHomeBinding.iconCloud,
        fragmentHomeBinding.iconHumidity,
        fragmentHomeBinding.iconPressure,
        fragmentHomeBinding.iconSunrise,
        fragmentHomeBinding.iconSunset
    )

    for (tv in textViewsIcon) {
        tv.typeface = weatherFontIcon
    }
}
