package net.solunar.app.solunarweather.di

import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import net.solunar.app.solunarweather.ui.home.HomeFragment
import net.solunar.app.solunarweather.ui.home.HomeViewModel
import javax.inject.Singleton

@Module
interface HomeFragmentModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    fun bindDemoViewModel(homeViewModel: HomeViewModel): ViewModel

}
