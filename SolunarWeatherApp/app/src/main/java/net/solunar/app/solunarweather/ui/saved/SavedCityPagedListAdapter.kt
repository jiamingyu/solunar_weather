package net.solunar.app.solunarweather.ui.saved

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import net.solunar.app.solunarweather.databinding.SavedCityCardBinding
import net.solunar.app.solunarweather.entity.meteoblue.City
import net.solunar.app.solunarweather.ui.search.DiffUtilCallBack
import javax.inject.Inject


class SavedCityPagedListAdapter @Inject constructor() : PagedListAdapter<City, SavedCityPagedListAdapter.SavedCityViewHolder>(
    DiffUtilCallBack()
) {

    lateinit var holderClickCurryFunc : (View) -> (City) -> Unit

    /**
     * @constructor viewBinding Binding object to provide view from its root
     *
     */
    inner class SavedCityViewHolder(val viewBinding: SavedCityCardBinding): RecyclerView.ViewHolder(viewBinding.root) {

        /**
         * @param City Item to support bound data. It must happen before rest of pending bindings happening
         */
        fun bind(city: City) {
            viewBinding.city = city
            viewBinding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SavedCityViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val savedCityCardBinding = SavedCityCardBinding.inflate(layoutInflater, parent, false)
        return SavedCityViewHolder(savedCityCardBinding)
    }

    override fun onBindViewHolder(holderSaved: SavedCityViewHolder, position: Int) {
        getItem(position)?.let { city ->
            holderSaved.apply {
                bind(city)
                itemView.setOnClickListener { view ->
                    if (::holderClickCurryFunc.isInitialized) {
                        holderClickCurryFunc(view)(city)
                    }
                }
            }
        }
    }
}
