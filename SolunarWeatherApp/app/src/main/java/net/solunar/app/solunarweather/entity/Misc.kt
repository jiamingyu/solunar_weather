package net.solunar.app.solunarweather.entity

import net.solunar.app.solunarweather.entity.meteoblue.City

sealed class LoveAction()
object UnLove: LoveAction()
object Love: LoveAction()

data class SaveCityMessage(val city: City, val action: LoveAction)
